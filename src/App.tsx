import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import Lesson1 from './components/lesson1';
import Lesson2 from './components/lesson2';
import { Provider } from 'react-redux';
import store from './redux/store'

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/" ><Lesson1 /></Route>
          <Route exact path="/lesson" ><Lesson2 /></Route>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
