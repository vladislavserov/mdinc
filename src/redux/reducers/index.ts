import { AnyAction } from "redux";

const initState = {
    lesson2Boxes: [
    {
      url: process.env.PUBLIC_URL + '/ko.png',
      id: '1',
    },
    {
      url: process.env.PUBLIC_URL + '/le.png',
      id: '2',
    },
    {
      url: process.env.PUBLIC_URL + '/so.png',
      id: '3',
    },
  ],
    lesson1Boxes: [
        {
            url: process.env.PUBLIC_URL + '/square.png',
            id: '1',
        },
        {
            url: process.env.PUBLIC_URL + '/oqtogone.png',
            id: '2',
        },
        {
            url: process.env.PUBLIC_URL + '/square.png',
            id: '3',
        },
        {
            url: process.env.PUBLIC_URL + '/oqtogone.png',
            id: '4',
        },
    ]
}

export const rootReducer = (state = initState, action: AnyAction) => {
    switch(action.type) {
        default:
            return state
    }
}