import { CSSProperties, FC } from 'react'
import { useDrop } from 'react-dnd'
import { DustbinProps } from './interfaces'
import { ItemTypes } from './ItemTypes'

const style: CSSProperties = {
  height: '5rem',
  width: '5rem',
  marginRight: '0.5rem',
  marginBottom: '0.5rem',
  color: 'white',
  padding: '1rem',
  textAlign: 'center',
  fontSize: '1rem',
  lineHeight: 'normal',
  float: 'left',
}


function selectBackgroundColor(isActive: boolean, canDrop: boolean) {
  if (isActive) {
    return 'darkgreen'
  } else if (canDrop) {
    return 'darkkhaki'
  } else {
    return '#222'
  }
}

export const Dustbin: FC<DustbinProps> = ({ allowedDropEffect, id, link }) => {
  const [{ canDrop, isOver }, drop] = useDrop(
    () => ({
      accept: ItemTypes.BOX,
      drop: () => ({
        name: id,
        allowedDropEffect,
      }),
      collect: (monitor: any) => ({
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
      }),
    }),
    [allowedDropEffect],
  )

  const isActive = canDrop && isOver
  const backgroundColor = selectBackgroundColor(isActive, canDrop)
  if (!link) {
    return (<div id={id} ref={drop} style={{ ...style, backgroundColor }} />)
  } else {
    return (<img src={link} id={id} ref={drop} style={{ ...style, }} />)
  }
}
