import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { useState } from 'react'
import { Container } from './Container'
import { useHistory } from 'react-router-dom'
import { connect } from "react-redux";


const Lesson2 = ({ lesson2Boxes }: any) => {

  const history = useHistory();
  const boxes = lesson2Boxes
  const [emptyBoxes, setEmptyBoxes] = useState([
    {
      url: '',
      id: '1',
    },
    {
      url: '',
      id: '2',
    },
    {
      url: '',
      id: '3',
    },
  ])
  const changeBoxBackgroand = (name: string, id: string) => {
    const newUrl = process.env.PUBLIC_URL + `/${name.toLowerCase()}.png`
    setEmptyBoxes(emptyBoxes => {
      const newBoxes = emptyBoxes.map((box, i) => {
        if (i + 1 === +id) {
          return { url: newUrl, id: box.id }
        } else {
          return box
        }
      })
      return newBoxes
    })
    return emptyBoxes
  }

  const check = () => {
    const answer = emptyBoxes.map((box, i) => emptyBoxes[i].url === boxes[i].url)
    if (answer.filter(item => item !== true).length === 0) {
      alert('good')
    } else {
      alert('bed')
    }
  }

  const toDefaultValues = () => {
    setEmptyBoxes([
      {
        url: '',
        id: '1',
      },
      {
        url: '',
        id: '2',
      },
      {
        url: '',
        id: '3',
      },
    ])
  }
  return (
    <div className="App">
      <h3>Составьте из слогов в зеленых ячейках слова</h3>
      <h4>составьте из слогов слово (колесо)</h4>
      <DndProvider backend={HTML5Backend}>
        <Container boxes={boxes} emptyBoxes={emptyBoxes} changeBoxBackgroand={changeBoxBackgroand} />
        <button onClick={() => check()}>test</button>
        <button onClick={() => toDefaultValues()}>Restart</button>
      </DndProvider>
      <button onClick={() => history.push('/')}>Next lesson</button>
    </div>
  )
}

const mapStateToProps = ({ lesson2Boxes }: any) => ({ lesson2Boxes });
export default connect(mapStateToProps)(Lesson2)