import { Dustbin } from './Dustbin'
import { Box } from './Box'

export const Container = ({ boxes, emptyBoxes, changeBoxBackgroand }: { boxes: any, emptyBoxes: any, changeBoxBackgroand(name: string, id: string): void }) => (

  <div>
    <div style={{ overflow: 'hidden', clear: 'both' }}>
      {boxes.map((box: any) => {
        return (
          <img src={box.url} alt={box.id} style={{ width: '5rem', height: '5rem' }} />
        )
      })}
      {emptyBoxes.map((box: any) => {
        return (
          <Dustbin allowedDropEffect='any' id={box.id} link={box.url} />
        )
      })}
    </div>

    <div style={{ overflow: 'hidden', clear: 'both' }}>
    </div>
    <div style={{ overflow: 'hidden', clear: 'both' }}>
      <Box name="Square" link={process.env.PUBLIC_URL + '/square.png'} changeBoxBackgroand={changeBoxBackgroand} />
      <Box name="Oqtogone" link={process.env.PUBLIC_URL + '/oqtogone.png'} changeBoxBackgroand={changeBoxBackgroand} />
    </div>
  </div>
)
