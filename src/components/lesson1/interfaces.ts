export interface DragItem {
    type: string
    name: string
  }
  
  export interface DropResult {
    name: string
    dropEffect: string
    allowedDropEffect: string
  }
  
export interface BoxProps {
  name: string
  link?: string
  changeBoxBackgroand(name: string, id: string): void
}

export interface DropResult {
  allowedDropEffect: string
  dropEffect: string
  name: string
}