import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { useState } from 'react'
import { Container } from './Container'
import { useHistory } from 'react-router-dom'
import { connect } from "react-redux";

const Lesson1 = ({ lesson1Boxes }: any) => {
    const history = useHistory();
    const boxes = lesson1Boxes
    const [emptyBoxes, setEmptyBoxes] = useState([
        {
            url: '',
            id: '1',
        },
        {
            url: '',
            id: '2',
        },
        {
            url: '',
            id: '3',
        },
        {
            url: '',
            id: '4',
        },
    ])
    const changeBoxBackgroand = (name: string, id: string) => {
        const newUrl = process.env.PUBLIC_URL + `/${name.toLowerCase()}.png`
        setEmptyBoxes(emptyBoxes => {
            const newBoxes = emptyBoxes.map((box, i) => {
                if (i + 1 === +id) {
                    return { url: newUrl, id: box.id }
                } else {
                    return box
                }
            })
            console.log(emptyBoxes, newBoxes)
            return newBoxes
        }
        )
        return emptyBoxes
    }

    const check = () => {
        const answer = emptyBoxes.map((box, i) => emptyBoxes[i].url === boxes[i].url)
        if (answer.filter(item => item !== true).length === 0) {
            alert('good')
        } else {
            alert('bed')
        }
    }
    const restart = () => {
        setEmptyBoxes([
            {
                url: '',
                id: '1',
            },
            {
                url: '',
                id: '2',
            },
            {
                url: '',
                id: '3',
            },
            {
                url: '',
                id: '4',
            },
        ])
    }

    return (
        <div className="App">
            <h3>Посмотрите на рисунок и продолжите его, выбрав один из элементов в зеленых ячейках.</h3>
            <h4>Посмотрите на рисунок, продолжите его</h4>
            <DndProvider backend={HTML5Backend}>
                <Container boxes={boxes} emptyBoxes={emptyBoxes} changeBoxBackgroand={changeBoxBackgroand} />
                <button onClick={() => check()}>test</button>
                <button onClick={() => restart()}>Restart</button>
                <button onClick={() => history.push('/lesson')}>Next lesson</button>
            </DndProvider>
        </div>
    )
}

const mapStateToProps = ({ lesson1Boxes }: any) => ({ lesson1Boxes });
export default connect(mapStateToProps)(Lesson1)